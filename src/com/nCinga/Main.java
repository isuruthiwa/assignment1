package com.nCinga;

public class Main {

    public static void getSequence(int[] numbers) {
        if(numbers.length>0){
            int[] sumArray= new int[numbers.length+1];

            //Get Sum of i and i+1
            for(int i=0; i<numbers.length+1;i++){
                if(i==0){
                    sumArray[i] = numbers[i];
                }
                else if(i<numbers.length) {
                    sumArray[i] = numbers[i] + numbers[i - 1];
                }
                else{
                    sumArray[i] = numbers[i-1];
                }
            }

            //Find even places and get the places with the highest length
            int patternStart=0;
            int patternEnd =0;
            int patternLength = 0;
            int lastPatternEnd=0;

            for(int i=0; i<sumArray.length;i++){
                if((sumArray[i]%2==0)||(i==sumArray.length-1)){
                    int length=i-lastPatternEnd;
                    if(length>patternLength){
                        patternStart=lastPatternEnd;
                        patternEnd=i;
                        patternLength=length;
                    }
                    lastPatternEnd=i;
                }
            }

            //Create the final array
            int index=0;
            int[] result= new int[patternLength];
            for(int i=patternStart;i<patternEnd;i++){
                result[index++]=numbers[i];
            }

            System.out.println("Start: "+patternStart);
            System.out.println("End: "+(patternEnd-1));

            for(int element: result){
                System.out.print(element+"\t");
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        int[] input=   {1, 2, 3, 4, 5, 6, 7, 8, 9, 4, 2, 7, 4 , 1, 8, 1, 2, 3, 4, 5, 6, 7, 8, 9, 4, 5,6 ,7, 8, 3};;
        getSequence(input);
    }
}
