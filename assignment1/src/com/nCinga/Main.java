package com.nCinga;

public class Main {

    public static void getSequence(int[] numbers){
        int[] result = null;
        int resultCount=0;
        //Code
        int start=0;
        for(int i=1; i<numbers.length;i++){
            if(((numbers[i-1]%2==0 && numbers[i]%2==1)||(numbers[i-1]%2==1 && numbers[i]%2==0)) && (i!=numbers.length-1)){
                continue;
            }
            else{
                if(resultCount<i-start){
                    int count=0;
                    result= new int[i-start];
                    for(int k=start; k<i; k++){
                        result[count++]=numbers[k];
                    }
                    resultCount=i-start;
                }
                start=i+1;
                //System.out.println(start);
            }
        }

        for(int element: result){
            System.out.print(element+"\t");
        }
    }

    public static void main(String[] args) {
	// write your code here
        int[] input={1,2,3,4,5,6,7,8,9,4,2,7,4,1,8,1,2,3,4,5,6,7,8,9,4,6,7,8,3};
        getSequence(input);
    }
}
